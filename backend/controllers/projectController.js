const asyncHandler = require('express-async-handler')
const Project = require('../models/projectModel')

const getAllProjects = asyncHandler(async (req, res) => {
    const projects = await Project.find({}).populate({
      'path': 'tasks',
      'select': 'name',
    })
    res.status(200).json({
      data: projects,
      success: true,
    })
}
)

const getProjectById = asyncHandler(async (req, res) => {
    const project = await Project.findById(req.params.id)
    if (project) {
      res.status(200).json({
        data: project,
        success: true,
      })
    }
    else {
      res.status(404).json({
        message: 'Project not found',
        success: false,
      })
      throw new Error('Project not found')
    }
}
)

const createProject = asyncHandler(async (req, res) => {
    const project = await Project.create(req.body)
    if (project) {
      res.status(201).json({
        data: project,
        success: true,
      })
    }
    else {
      res.status(400).json({
        message: 'Invalid project data',
        success: false,
      })
      throw new Error('Invalid project data')
    }
}
)

const updateProject = asyncHandler(async (req, res) => {
    const project = await Project.findById(req.params.id)
    if (project) {
      project.name = req.body.name || project.name
      project.description = req.body.description || project.description
      project.completed = req.body.completed || project.completed
      const updatedProject = await project.save()
      res.status(200).json({
        data: updatedProject,
        success: true,
      })
    }
    else {
      res.status(404).json({
        message: 'Project not found',
        success: false,
      })
      throw new Error('Project not found')
    }
}
)

const deleteProject = asyncHandler(async (req, res) => {
    const project = await Project.findById(req.params.id)
    if (project) {
      await project.deleteOne()
      res.status(200).json({
        message: 'Project removed',
        success: true,
      })
    }
    else {
      res.status(404).json({
        message: 'Project not found',
        success: false,
      })
      throw new Error('Project not found')
    }
}
)

module.exports = { getAllProjects, getProjectById, createProject, updateProject, deleteProject }
