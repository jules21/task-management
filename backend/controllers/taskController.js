const asyncHandler = require('express-async-handler')
const Task = require('../models/taskModel')
const Project = require('../models/projectModel')
const User = require('../models/userModel')
const multer = require('multer')
const path = require('path')

const getAllTasks = asyncHandler(async (req, res) => {
  const tasks = await Task.find({})
  .populate({
    'path': 'assignees',
    'select': 'name',

  })
  .populate({
    'path': 'projects',
    'select': 'name',

  })
  .exec()
  res.status(200).json({
    data: tasks,
    success: true,
  })
})

const getTaskById = asyncHandler(async (req, res) => {
  const task = await Task.findById(req.params.id)
  if (task) {
    res.status(200).json({
      data: task,
      success: true,
    })
  } else {
    res.status(404).json({
      message: 'Task not found',
      success: false,
    })
    throw new Error('Task not found')
  }
})

const createTask = asyncHandler(async (req, res, next) => {
  //remove the projects and assignees from the req.body
  const { projects, assignees, ...rest } = req.body
  const attachmentFile = req.file

  if (attachmentFile) {
    const fileName = attachmentFile.originalname
    const ext = path.extname(fileName)
    const allowedExt = ['.jpg', '.jpeg', '.png', '.pdf']
    if (!allowedExt.includes(ext)) {
      res.status(400)
      throw new Error('Only images and pdfs are allowed')
    }
  }

  const fileName = attachmentFile ? attachmentFile.originalname : null
  req.body.attachment = fileName



  //create the task
  let task = await Task.create(req.body)
  //add the projects and assignees to the task

  if (projects) {
    projects.forEach(async (projectId) => {
      let project = await Project.findById(projectId)
      if (project) {
        project.tasks.push(task)
        project.save()
        // task.projects.push(project)
      } else {
        console.log('project not found')
      }
    })
  }
  //return the task

  if (task) {
    res.status(201).json({
      data: task,
      success: true,
    })
  } else {
    res.status(400).json({
      message: 'Invalid task data',
      success: false,
    })
    throw new Error('Invalid task data')
  }
})

const updateTask = asyncHandler(async (req, res) => {
  const task = await Task.findById(req.params.id)
  if (task) {
    const updatedTask = await task.save()
    res.status(200).json({
      data: updatedTask,
      success: true,
    })
  } else {
    res.status(404).json({
      message: 'Task not found',
      success: false,
    })
    throw new Error('Task not found')
  }
})

const deleteTask = asyncHandler(async (req, res) => {
  const task = await Task.findById(req.params.id)
  if (task) {
    await task.deleteOne()
    res.status(200).json({
      message: 'Task removed',
      success: true,
    })
  } else {
    res.status(404).json({
      message: 'Task not found',
      success: false,
    })
    throw new Error('Task not found')
  }
})


module.exports = {
  getAllTasks,
  getTaskById,
  createTask,
  updateTask,
  deleteTask,
}
