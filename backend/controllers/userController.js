const User = require('../models/userModel')
const asyncHandler = require('express-async-handler')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const Token = require('../models/tokenModel')
const crypto = require("crypto");
const sendEmail = require('./mailController')


const registerUser = asyncHandler(async (req, res) => {
    const { name, email, phone, password } = req.body
  
    if (!name || !email || !phone || !password) {
      res.status(400).json({
        success: false,
        message: 'Please enter all fields',
      })
    }
  
    // Check if user exists
    const userExists = await User.findOne({ email })
  
    if (userExists) {
      res.status(400).json({
        success: false,
        message: 'User already exists',
      })
    }
  
    // Hash password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)
  
    // Create user
    const user = await User.create({
      name,
      email,
      phone,
      password: hashedPassword,
    })
  
    if (user) {
      res.status(201).json({
        _id: user.id,
        name: user.name,
        email: user.email,
        phone: user.phone,
        token: generateToken(user._id),
        success: true,
      })
    } else {
      res.status(400).json({
        success: false,
        message: 'Invalid user data',
      })
    }
  })

  const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body
  
    // Check for user email
    const user = await User.findOne({ email })
  
    if (user && (await bcrypt.compare(password, user.password))) {
      res.json({
        _id: user.id,
        name: user.name,
        email: user.email,
        token: generateToken(user._id),
        success: true,
      })
    } else {
      res.status(401).json({
        success: false,
        message: 'Invalid email or password',
      })
    }
  })

  const userProfile = asyncHandler( async (req, res)=>{
    res.status(200).json(req.user)
  })

  const requestResetPassword = asyncHandler( async (req, res)=>{
    const { email } = req.body

    const user = await User.findOne({ email });

    if (!user) res.status(400).json({ message: "User does not exist" });

    let token = await Token.findOne({ userId: user._id });

    if (token) await token.deleteOne();

    let resetToken = crypto.randomBytes(32).toString("hex");

    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(resetToken, Number(salt));
  
    await new Token({
      userId: user._id,
      token: hash,
      createdAt: Date.now(),
    }).save();
  
    const link = `127.0.0.1:5000/passwordReset?token=${resetToken}&id=${user._id}`;
    sendEmail(user.email,"Password Reset Request",{name: user.name,link: link,},"./template/requestResetPassword.handlebars");
    // return link;

    return res.json({ message: "Password reset link sent successfully" ,success: true});
  })

    const resetPassword = asyncHandler( async (req, res)=>{

    const { userId, token, password } = req.body
    let passwordResetToken = await Token.findOne({ userId });
  
    if (!passwordResetToken) {
      res.status(400).json({'success': false, message: "Invalid or expired password reset token"});
    }
  
    console.log(passwordResetToken.token, token);
  
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
  
    if (!isValid) {
      res.status(400).json({'success': false, message: "Invalid or expired password reset token"});
    }
  
    const hash = await bcrypt.hash(password, Number(bcryptSalt));
  
    await User.updateOne(
      { _id: userId },
      { $set: { password: hash } },
      { new: true }
    );
  
    const user = await User.findById({ _id: userId });
  
    await passwordResetToken.deleteOne();
    return res.status(200).json({ message: "Password reset successful", success: true });
    }
)

const logoutUser = asyncHandler(async (req, res) => {
    res.cookie('token', 'none', {
      expires: new Date(Date.now() + 10 * 1000),
      httpOnly: true,
    })
  
    res.status(200).json({
      success: true,
      data: {},
    })
  }
  )

const getAllUsers = asyncHandler(async (req, res) => {
    const users = await User.find({})
    res.status(200).json({
      data: users,
      success: true,
    })
  })

  // Generate JWT
const generateToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
      expiresIn: '30d',
    })
  }

  module.exports = {
    registerUser,loginUser, userProfile, resetPassword, requestResetPassword, getAllUsers,logoutUser
  }