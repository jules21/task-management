const mongoose = require('mongoose')

const projectSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please add a name'],
      maxlength: [150, 'Name can not be more than 50 characters']
    },
    description: {
      type: String,
      maxlength: [500, 'Description can not be more than 500 characters']
    },

    tasks: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task',
      },
    ],
  },
  {
    timestamps: true,
  }
)

module.exports = mongoose.model('Project', projectSchema)
