const mongoose = require('mongoose')

const taskSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please add a name'],
      maxlength: [150, 'Name can not be more than 50 characters']
    },
    startdate: {
        type: Date,
        required: [true, 'Please add a start date'],
        },
    enddate: {
        type: Date,
        required: [true, 'Please add a end date'],
        },
        description:{
            type:String,
            required: [true, 'Please add a description'],
            maxlength: [500, 'Description can not be more than 500 characters']
        },
        priority:{
            type:String,
            required: [true, 'Please add a priority'],
        },
        status:{
            type:String,
            default:"Draft",
        },
        attachment:{
            type:String,
        },
        assignees:[
            {
                type:mongoose.Schema.Types.ObjectId,
                ref:'User',
            }
        ],
        projects:[
            {
                type:mongoose.Schema.Types.ObjectId,
                ref:'Project',
            }
        ],
    },
  {
    timestamps: true,
  }
)

module.exports = mongoose.model('Task', taskSchema)
