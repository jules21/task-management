const express = require('express')
const router = express.Router()
const {getAllTasks, getTaskById, createTask, updateTask, deleteTask} = require('../controllers/taskController')
const { auth } = require('../middleware/auth')


router.route('/').get(auth, getAllTasks).post(auth, createTask)
router.route('/:id').get(auth, getTaskById).put(auth, updateTask).delete(auth, deleteTask)

module.exports = router
