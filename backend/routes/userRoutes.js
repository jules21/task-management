const express = require('express')
const router = express.Router()
const {registerUser, loginUser, userProfile, resetPassword, requestResetPassword,getAllUsers,logoutUser} = require('../controllers/userController')
const { auth } = require('../middleware/auth')


router.get('/', getAllUsers)
router.post('/', registerUser)
router.post('/login', loginUser)
router.get('/profile', auth, userProfile)
router.post('/request-reset-password', requestResetPassword)
router.post('/reset-password', resetPassword)
router.post('/logout', logoutUser)



module.exports = router
