const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
// Load models
const Projects = require('./models/projectModel');
const Tasks = require('./models/taskModel');
const dotenv = require('dotenv').config();


mongoose.connect("mongodb+srv://jules21:qwerty21@cluster0.qagapxd.mongodb.net/mgmt_db");

// Read JSON files
const projects = JSON.parse(
  fs.readFileSync(`${__dirname}/data/projects.json`, 'utf-8')
);

const tasks = JSON.parse(
  fs.readFileSync(`${__dirname}/data/tasks.json`, 'utf-8')
);


// Import into DB
const importData = async () => {
  try {
    await Projects.create(projects);
    await Tasks.create(tasks);

    console.log('Data Imported...'.green.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

if (process.argv[2] === '-i') {
  importData();
}
