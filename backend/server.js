const express = require('express');
const colors = require('colors');
const dotenv = require('dotenv').config();
const cor = require('cors');
const connectDB = require('./config/db');
const port = process.env.PORT || 5000;

connectDB();




const app = express();
app.use(cor());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/users', require('./routes/userRoutes'));
app.use('/api/tasks', require('./routes/taskRoutes'));
app.use('/api/projects', require('./routes/projectRoutes'));

app.listen(port, () => console.log(`Server started on port ${port}`));
