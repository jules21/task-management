import './App.css';
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Dashboard from './components/Dashboard';
import Tasks from './components/tasks/Tasks';
import Projects from './components/admin/Projects';
import Assignees from './components/admin/Assignees';
import NewTask from './components/tasks/NewTask';
import Profile from './components/auth/Profile';



function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/register" element={<Register />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/tasks/index" element={<Tasks />} />
        <Route path="/projects/index" element={<Projects />} />
        <Route path="/assignees/index" element={<Assignees />} />
        <Route path="*" element={<h1>Not Found</h1>} />
        <Route path="/tasks/new" element={<NewTask />} />
        <Route path="/tasks/:id/edit" element={<NewTask />} />
        <Route path="/profile" element={<Profile />} />

      </Routes>
      </Router>
  );
}

export default App;
