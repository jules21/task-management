import React from 'react'
import Layout from '../components/layout/Master'
import { Box, Container, SimpleGrid } from '@chakra-ui/react'
import { Stat } from './Stats'
    import { useEffect, useState } from 'react'
import {server} from '../config/constants'
import axios from 'axios'
const stats = [
  {
    label: 'Total Projects',
    value: '7',
  },
  {
    label: 'Total Tasks',
    value: '56',
  },
  {
    label: 'Total Users',
    value: '12',
  },
]

const Dashboard = () => {
    const [taskCount, setTaskCount] = useState(0)
    const [projectCount, setProjectCount] = useState(0)
    const [userCount, setUserCount] = useState(0)

    useEffect(() => {
      const getTasksCount = async () => {
        const tasksFromServer = await fetchTasksCount()
        setTaskCount(tasksFromServer)
      }
      getTasksCount()
      const getProjectsCount = async () => {
        const projectsFromServer = await fetchProjectsCount()
        setProjectCount(projectsFromServer)
      }
      getProjectsCount()
      const getUsersCount = async () => {
        const usersFromServer = await fetchUsersCount()
        setUserCount(usersFromServer)
      }
      getUsersCount()


    }, [])
      // Fetch Tasks
  const fetchTasksCount = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/tasks`)
    return res.data.data.length
    
  }
  // Fetch Projects
  const fetchProjectsCount = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/projects`)
    return res.data.data.length
    
  }
  
  // Fetch Users
  const fetchUsersCount = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/users`)
    return res.data.data.length
    
  }

  //update stats
  stats[0].value = projectCount
  stats[1].value = taskCount
  stats[2].value = userCount
  
  
  return (
    <Layout>
        <Box
    as="section"
    py={{
      base: '4',
      md: '8',
    }}
  >
    <Container>
      <SimpleGrid
        columns={{
          base: 1,
          md: 3,
        }}
        gap={{
          base: '5',
          md: '6',
        }}
      >
        {stats.map(({ label, value }) => (
          <Stat key={label} label={label} value={value} />
        ))}
      </SimpleGrid>
    </Container>
  </Box>
    </Layout>
  )
}

export default Dashboard
