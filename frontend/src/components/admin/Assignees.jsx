import React,{useRef} from 'react'
import Layout from '../layout/Master'
import axios from 'axios'
import moment from 'moment'
import {server} from '../../config/constants'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { FaFileExcel } from 'react-icons/fa'
import { FaPlus } from 'react-icons/fa'
import { DownloadTableExcel } from 'react-export-table-to-excel';
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Heading,
  Wrap,
  Button,
  Box
} from '@chakra-ui/react'
const Users = () => {

  const [users, setUsers] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const getUsers = async () => {
      setLoading(true)
      const usersFromServer = await fetchUsers()
      console.log(usersFromServer);
      setLoading(false)
      setUsers(usersFromServer)
    }

    getUsers()
  }, [])

  // Fetch Users
  const fetchUsers = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/users`)
    return res.data.data
    
  }

  const tableRef = useRef(null);
  return (
    <Layout>    
        <Wrap spacing='30px' justify='space-between' align='center'>
        <Heading as="h1" size="xl" color="primary.500" mb={2}> All Users </Heading>
        <Box>
        <DownloadTableExcel
                            filename="users table"
                            sheet="users"
                            currentTableRef={tableRef.current}
                        >
        <Button colorScheme="green" size="md" variant="solid" mr={2} leftIcon={<FaFileExcel />} variant='outline'>
            <Link to='#'>Export</Link>
        </Button>
        </DownloadTableExcel>
        </Box>
        </Wrap>
        <TableContainer>
  <Table variant='simple' border={'1px solid #ccc'} boxShadow={'sm'} borderRadius={'lg'} ref={tableRef} my={4}>
    <TableCaption>All Users List</TableCaption>
    <Thead>
      <Tr>
        <Th>Name</Th>
        <Th>Email</Th>
        <Th>Phone</Th>
        <Th>Creation Date</Th>
      </Tr>
    </Thead>
    <Tbody>
    {users.map((user, index) => (
      <Tr key={index}>
      <Td>{user.name}</Td>
      <Td>{user.email}</Td>
      <Td>{user.phone}</Td>
      <Td>{moment(user.createdAt).format('DD/MM/YYYY')}</Td>
    </Tr>
    ))}
    </Tbody>
    <Tfoot>
    <Tr>
       <Th>Name</Th>
        <Th>Email</Th>
        <Th>Phone</Th>
        <Th>Creation Date</Th>
      </Tr>
    </Tfoot>
  </Table>
</TableContainer>
    </Layout>
  )
}

export default Users
