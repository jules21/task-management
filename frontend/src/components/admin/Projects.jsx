import React, { useRef } from 'react'
import Layout from '../layout/Master'
import axios from 'axios'
import moment from 'moment'
import { server } from '../../config/constants'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { FaFileExcel } from 'react-icons/fa'
import { FaPlus } from 'react-icons/fa'
import { DownloadTableExcel } from 'react-export-table-to-excel'
import { useForm } from 'react-hook-form'
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Heading,
  Wrap,
  Button,
  Box,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Textarea,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Stack,


} from '@chakra-ui/react'
const Projects = () => {
  const [projects, setProjects] = useState([])
  const [loading, setLoading] = useState(false)
  const { isOpen, onOpen, onClose } = useDisclosure()

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors, isSubmitting },
  } = useForm()

  useEffect(() => {
    const getProjects = async () => {
      setLoading(true)
      const projectsFromServer = await fetchProjects()
      setProjects(projectsFromServer)
      setLoading(false)
    }

    getProjects()
  }, [])

  const onSubmit = (data) => createProject(data)

  const createProject = (data) => {
 
    const route =`${server}/api/projects`

    axios
      .post(route, {
        name: data.name,
        description: data.description,
      })
      .then((response) => {
        window.location.href = '/projects/index'
        this.addNotification(response.data.message)
      })
      .catch((error) => {
        console.log(error)
      })
  }

  // Fetch Projects
  const fetchProjects = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/projects`)
    return res.data.data
  }

  //Delete Project
  const deleteProject = async (id) => {
    alert('Are you sure you want to delete this project?')

    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    await axios.delete(`${server}/api/projects/${id}`)
    setProjects(projects.filter((project) => project._id !== id))
  }
  const tableRef = useRef(null)

  return (
    <Layout>
      <Wrap spacing="30px" justify="space-between" align="center">
        <Heading as="h1" size="xl" color="primary.500" mb={2}>
          {' '}
          All Projects{' '}
        </Heading>
        <Box>
          <DownloadTableExcel
            filename="users table"
            sheet="users"
            currentTableRef={tableRef.current}
          >
            <Button
              colorScheme="green"
              size="md"
              variant="solid"
              mr={2}
              leftIcon={<FaFileExcel />}
              variant="outline"
            >
              <Link to="#">Export</Link>
            </Button>
          </DownloadTableExcel>
          <Button
            colorScheme="blue"
            size="md"
            variant="solid"
            leftIcon={<FaPlus />}
            variant="outline"
            onClick={onOpen}
          >
            <Link to="#">New Project</Link>
          </Button>
        </Box>
      </Wrap>
      <TableContainer>
        <Table
          variant="simple"
          border={'1px solid #ccc'}
          boxShadow={'sm'}
          borderRadius={'lg'}
          ref={tableRef}
          my={4}
        >
          <TableCaption>All Projects List</TableCaption>
          <Thead>
            <Tr>
              <Th>Name</Th>
              <Th>Description</Th>
              <Th>Tasks</Th>
              <Th>Creation Date</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
          <Tbody>
            {projects.map((project, index) => (
              <Tr key={index}>
                <Td>{project.name}</Td>
                <Td>{project.description}</Td>
                <Td>
                  {project.tasks &&
                    project.tasks.map((task, index) => (
                      <span key={index}>{task.name}</span>
                    ))}
                  {projects.length - 1 !== index && <span>, </span>}
                </Td>
                <Td>{moment(project.createdAt).format('DD/MM/YYYY')}</Td>
                <Td>
                  {/* <Button colorScheme="blue" size="sm" variant="solid">
                    <Link to={`/tasks/${task.id}`}>View</Link>
                </Button> */}
                  <Button colorScheme="blue" size="sm" mr={2} variant="solid">
                    <Link to={`/tasks/${project._id}/edit`}>Edit</Link>
                  </Button>
                  <Button
                    colorScheme="red"
                    size="sm"
                    variant="solid"
                    onClick={() => deleteProject(project._id)}
                  >
                    <Link>Delete</Link>
                  </Button>
                </Td>
              </Tr>
            ))}
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>Name</Th>
              <Th>Description</Th>
              <Th>Tasks</Th>
              <Th>Creation Date</Th>
              <Th>Actions</Th>
            </Tr>
          </Tfoot>
        </Table>
      </TableContainer>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>New  Project</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Stack spacing={4}>
              {/* name */}
              <FormControl id="name" isInvalid={errors.name} isRequired>
                <FormLabel>Name</FormLabel>
                <Input 
                
                  type="text"
                  {...register('name', {
                    required: 'This is required',
                  })}
                />
                <FormErrorMessage>
                  {errors.name && errors.name.message}
                </FormErrorMessage>
              </FormControl>

              {/* Description must no be greater than 100 characters, show yellow on 55 red on 80 */}
              <FormControl id="description" isInvalid={errors.description}>
                <FormLabel>Description</FormLabel>
                <Textarea
                  type="text"
                  {...register('description', {
                    required: 'This is required',
                  })}
                />
                <FormErrorMessage>
                  {errors.description && errors.description.message}
                </FormErrorMessage>
              </FormControl>
            </Stack>
          </form>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme='blue' mr={3} onClick={onClose}>
              Close
            </Button>
            <Button variant='ghost' type='submit' isLoading={isSubmitting} onClick={handleSubmit(onSubmit)}>Save</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Layout>
  )
}

export default Projects
