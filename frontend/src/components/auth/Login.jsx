'use client'

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Checkbox,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  FormErrorMessage,
} from '@chakra-ui/react'
import { useState } from 'react'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import {server} from '../../config/constants'
import { Link } from 'react-router-dom'
export default function Login() {

    const {
        register,
        handleSubmit,
        watch,
        formState: { errors,isSubmitting },
      } = useForm()

      const onSubmit = (data) => logginUser(data)

    const logginUser = ({email, password}) => {
        axios
          .post(`${server}/api/users/login`, {
            email: email,
            password: password,
          })
          .then((response) => {
            if(response?.data?.token) {
              localStorage.setItem('token', response.data.token)
              window.location.href = '/dashboard';
            }
          }).catch((e) => {
            //get status code
            if(e?.response?.status === 401) {
              window.alert("Email or password is incorrect")
            }
          })
      }

  return (
    <Flex
      minH={'100vh'}
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
        <Stack align={'center'}>
          <Heading fontSize={'4xl'}>Sign in to your account</Heading>
          <Text fontSize={'lg'} color={'gray.600'}>
            No account? create  <Link to='/register' color='blue'>here</Link>
          </Text>
        </Stack>
        <Box
          rounded={'lg'}
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'lg'}
          p={8}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Stack spacing={4}>
            <FormControl id="email" isInvalid={errors.email}>
              <FormLabel>Email address</FormLabel>
              <Input type="email" name='email' 
                // onChange={(e) => setEmail(e.target.value)} 
                {...register('email', { required: true, })} />
            <FormErrorMessage>
                {errors.email && "Email is required"}
            </FormErrorMessage>
            </FormControl>

            <FormControl id="password" isInvalid={errors.password}>
              <FormLabel>Password</FormLabel>
              <Input type="password" name='password' 
                // onChange={(e) => setPassword(e.target.value)} 
                {...register('password', { required: true, })} />
            <FormErrorMessage>
                {errors.password && "Password is required"}
            </FormErrorMessage>
            </FormControl>
            <Stack spacing={10}>
              <Stack
                direction={{ base: 'column', sm: 'row' }}
                align={'start'}
                justify={'space-between'}>
                <Checkbox>Remember me</Checkbox>
                <Text color={'blue.400'}>Forgot password?</Text>
              </Stack>
              <Button isLoading={isSubmitting} type='submit'
                bg={'blue.400'}
                color={'white'}
                _hover={{
                  bg: 'blue.500',
                }}>
                Sign in
              </Button>
            </Stack>
          </Stack>
          </form>
        </Box>
      </Stack>
    </Flex>
  )
}