'use client'

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  HStack,
  InputRightElement,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  Link as ChakraLink,
  FormErrorMessage,
} from '@chakra-ui/react'
import { useState } from 'react'
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons'
import {server} from '../../config/constants'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import {Link } from 'react-router-dom'

export default function Register() {
  const [showPassword, setShowPassword] = useState(false)

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors,isSubmitting },
  } = useForm()

  const onSubmit = (data) => registerUser(data)


  const registerUser = (data) => {
    const {firstName, lastName, phone, email, password} = data
    axios
      .post(`${server}/api/users`, {
        name: firstName + " " + lastName,
        phone: phone,
        email: email,
        password: password,
      })
      .then((response) => {
        if(response?.data?.token) {
          localStorage.setItem('token', response.data.token)
          window.location.href = '/dashboard';
        }
      }).catch((e) => {
        console.log(" err > ", e?.response?.data);
        window.alert(e?.response?.data?.message);
      })
  }

  return (
    <Flex
      minH={'100vh'}
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
        <Stack align={'center'}>
          <Heading fontSize={'4xl'} textAlign={'center'}>
            Sign up
          </Heading>
          <Text fontSize={'lg'} color={'gray.600'}>
            to create an account
          </Text>
        </Stack>
        <Box
          rounded={'lg'}
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'lg'}
          p={8}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Stack spacing={4}>
            <HStack>
              <Box>
                <FormControl id="firstName" isRequired isInvalid={errors.firstName}>
                  <FormLabel>First Name</FormLabel>
                  <Input type="text"   name='firstName' {...register('firstName', { required: true })} />
                  <FormErrorMessage
                    mt={0}
                    position={'absolute'}
                    right={'0'}>
                    {errors.firstName && "First Name is required"}
                    </FormErrorMessage>
                </FormControl>
              </Box>
              <Box>
                <FormControl id="lastName" isRequired isInvalid={errors.lastName}>
                  <FormLabel>Last Name</FormLabel>
                  <Input type="text" name='lastName' {...register('lastName', { required: true })} />
                    <FormErrorMessage mt={0} position={'absolute'} right={'0'}>
                        {errors.lastName && "Last Name is required"}
                    </FormErrorMessage>
                </FormControl>
              </Box>
            </HStack>
            <FormControl id="email" isRequired isInvalid={errors.email}>
              <FormLabel>Email address</FormLabel>
              <Input type="email" name='email' {...register('email', { required: true })} />
                <FormErrorMessage mt={0} position={'absolute'} right={'0'}>
                    {errors.email && "Email is required"}
                </FormErrorMessage>
            </FormControl>
            <FormControl id="phone" isRequired isInvalid={errors.phone}>
                <FormLabel>Phone</FormLabel>
                <Input type="text" name='phone' {...register('phone', { required: true })} />
                <FormErrorMessage mt={0} position={'absolute'} right={'0'}>
                    {errors.phone && "Phone is required"}
                </FormErrorMessage>
            </FormControl>
            <FormControl id="password" isRequired isInvalid={errors.password}>
              <FormLabel>Password</FormLabel>
              <InputGroup>
                <Input type={showPassword ? 'text' : 'password'} name='password' {...register('password', { required: true })} />
                <InputRightElement h={'full'}>
                  <Button
                    variant={'ghost'}
                    onClick={() => setShowPassword((showPassword) => !showPassword)} isLoading={isSubmitting} type='submit'>
                    {showPassword ? <ViewIcon /> : <ViewOffIcon />}
                  </Button>
                </InputRightElement>
              </InputGroup>
                <FormErrorMessage mt={0} position={'absolute'} right={'0'}>
                    {errors.password && "Password is required"}
                </FormErrorMessage>
            </FormControl>
            <Stack spacing={10} pt={2}>
            <Button isLoading={isSubmitting} type='submit'
                bg={'blue.400'}
                color={'white'}
                _hover={{
                  bg: 'blue.500',
                }}>
                Sign up
              </Button>
            </Stack>
            <Stack pt={6}>
              <Text align={'center'}>
                Already a user? 
                
                    <Link to={'/login'}>
                        <span style={{color: 'blue'}}> Login</span>
                
                        </Link>
                  
              </Text>
            </Stack>
          </Stack>
          </form>
        </Box>
      </Stack>
    </Flex>
  )
}