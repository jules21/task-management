import React from 'react'
import Navbar from './Navbar'

import { Container, Box } from '@chakra-ui/react'

const master = ({children}) => {
  return (
    <>
      <Navbar />
        <Container maxW="container.xl" mt={10}>
            {children}
        </Container>
    </>
  )
}

export default master
