import { useState, useEffect } from 'react'
import {useParams} from 'react-router-dom'
import  moment from 'moment'
import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  HStack,
  InputRightElement,
  Stack,
  Button,
  Heading,
  Text,
  FormErrorMessage,
  Wrap,
  Radio,
  RadioGroup,
  useColorModeValue,
  Container,
  SimpleGrid,
  Center,
  Textarea,
} from '@chakra-ui/react'

import { server } from '../../config/constants'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { Link } from 'react-router-dom'
import Layout from '../layout/Master'
import Select from 'react-select'

const NewTask = () => {
  const [projectsOptions, setProjectsOptions] = useState([])
  const [assigneesOptions, setAssigneesOptions] = useState([])
  const [assignees, setAssignees] = useState([])
  const [projects, setProjects] = useState([])
  const [value, setValue] = useState('1')
  const [status, setStatus] = useState('Draft')
  const [task,setTask] = useState([]);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors, isSubmitting },
  } = useForm()
  const id = useParams().id

  useEffect(() => {
    const getTask = async () => {
      const taskFromServer = await fetchTask(id)
      setTask(taskFromServer)
    }
    getTask()
    const getAssignees = async () => {
      const assigneesFromServer = await fetchAssignees()
      const options1 = assigneesFromServer.map((assignee) => {
        return { value: assignee._id, label: assignee.name }
      })
      setAssigneesOptions(options1)
    }
    const getProjects = async () => {
      const projectsFromServer = await fetchProjects()
      const options2 = projectsFromServer.map((project) => {
        return { value: project._id, label: project.name }
      })
      setProjectsOptions(options2)
    }
    getAssignees()
    getProjects()
  }, [id])

  // // Fetch Task
  const fetchTask = async (id) => {
    console.log(id);
    if (!id) return

    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/tasks/${id}`)
    return res.data.data
  }

  // Fetch Assignees
  const fetchAssignees = async () => {
    //passing token in header

    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/users`)
    return res.data.data
  }
  // Fetch Projects
  const fetchProjects = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/projects`)
    return res.data.data
  }

  const onSubmit = (data) => createTask(data)

  const createTask = (data) => {
    const assigneesIds = assignees.map((assignee) => {
      return assignee.value
    })
    const projectsIds = projects.map((project) => {
      return project.value
    })

    const route = id ? `${server}/api/tasks/${id}` : `${server}/api/tasks`

    axios
      .post(route, {
        name: data.name,
        description: data.description,
        startdate: data.start_date,
        enddate: data.end_date,
        priority: value,
        assignees: assigneesIds,
        projects: projectsIds,
        attachment: data.attachment,
        // attachment: 'attachment1',
        status: status,
      })
      .then((response) => {
        window.location.href = '/tasks/index'
        this.addNotification(response.data.message)
      })
      .catch((error) => {
        console.log(error)
      })
  }
  return (
    <Layout>
      <Center>
        <Box w={[300, 400, 500]}>
          <Wrap spacing="30px" justify="space-between" align="center">
            <Heading as="h1" size="md" color="primary.500" mb={2}>
              Create Task
            </Heading>
            <Button colorScheme="blue" size="md" variant="solid" variant='outline'>
              <Link to="/tasks/new">Save Draft</Link>
            </Button>
          </Wrap>
          <form onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" name="id" value={id} />
            <Stack spacing={4}>
              {/* name */}
              <FormControl id="name" isInvalid={errors.name} isRequired>
                <FormLabel>Name</FormLabel>
                <Input 
                  value={task && task.name}
                  type="text"
                  {...register('name', {
                    required: 'This is required',
                  })}
                />
                <FormErrorMessage>
                  {errors.name && errors.name.message}
                </FormErrorMessage>
              </FormControl>
              {/* start date */}
              <HStack spacing={4}>
                <Box flex={1}>
                  <FormControl
                    id="start_date"
                    isInvalid={errors.start_date}
                    isRequired
                  >
                    <FormLabel>Start Date</FormLabel>
                    <Input
                    value={task && moment(task.startdate).format('YYYY-MM-DD')}
                      type="date"
                      {...register('start_date', {
                        required: 'This is required',
                      })}
                    />
                    <FormErrorMessage>
                      {errors.start_date && errors.start_date.message}
                    </FormErrorMessage>
                  </FormControl>
                </Box>
                {/* end date */}
                <Box flex={1}>
                  <FormControl
                    id="end_date"
                    isInvalid={errors.end_date}
                    isRequired
                  >
                    <FormLabel>End Date</FormLabel>
                    <Input

                      value={task && moment(task.enddate).format('YYYY-MM-DD')}
                      type="date"
                      {...register('end_date', {
                        required: 'This is required',
                      })}
                    />
                    <FormErrorMessage>
                      {errors.end_date && errors.end_date.message}
                    </FormErrorMessage>
                  </FormControl>
                </Box>
              </HStack>
              {/* assignees must came from somewhere  and loop through  */}
              <FormControl id="assignees" isInvalid={errors.assignees}>
                <FormLabel>Assignees</FormLabel>
                <Select
                  options={assigneesOptions}
                  isMulti
                  onChange={(e) => setAssignees(e)}
                />
                <FormErrorMessage>
                  {errors.assignees && errors.assignees.message}
                </FormErrorMessage>
              </FormControl>
              {/* assignees must came from somewhere  and loop through  */}
              <FormControl id="assignees" isInvalid={errors.assignees}>
                <FormLabel>Projects</FormLabel>
                <Select
                  options={projectsOptions}
                  isMulti
                  onChange={(e) => setProjects(e)}
                />
                <FormErrorMessage>
                  {errors.assignees && errors.assignees.message}
                </FormErrorMessage>
              </FormControl>

              {/* Description must no be greater than 100 characters, show yellow on 55 red on 80 */}
              <FormControl id="description" isInvalid={errors.description}>
                <FormLabel>Description</FormLabel>
                <Textarea
                  value={task && task.description}
                  type="text"
                  {...register('description', {
                    required: 'This is required',
                  })}
                />
                <FormErrorMessage>
                  {errors.description && errors.description.message}
                </FormErrorMessage>
              </FormControl>

              {/* Priority  low, normal, high */}
              <RadioGroup
                onChange={setValue}
                value={value}
                value="Normal"
              >
                <FormLabel>Priority</FormLabel>
                <Stack direction="row">
                  <Radio value="Low">Low</Radio>
                  <Radio value="Normal" defaultChecked>
                    Normal
                  </Radio>
                  <Radio value="High">High</Radio>
                </Stack>
              </RadioGroup>

              <RadioGroup
                onChange={setStatus}
                value={status}
                value="Pending"
              >
                <FormLabel>Status</FormLabel>
                <Stack direction="row">
                  <Radio value="Pending">Pending</Radio>
                  <Radio value="In Progress">In Progress</Radio>
                  <Radio value="Completed">Completed</Radio>
                </Stack>
              </RadioGroup>

              <Wrap spacing="30px" justify="space-between" align="center">
                {/* Attachment*/}
                <FormControl
                  id="attachment"
                  isInvalid={errors.attachment}
                  flex={1}
                >
                  <FormLabel>Attachment</FormLabel>
                  <Input
                    type="file"
                    {...register('attachment', {
                      required: 'This is required',
                    })}
                  />
                  <FormErrorMessage>
                    {errors.attachment && errors.attachment.message}
                  </FormErrorMessage>
                </FormControl>
                <HStack spacing={4}>
                  <Box flex={1}>
                    {/* cancel button */}
                    <Button colorScheme="red" size="md" variant="solid">
                      <Link to="/tasks">Cancel</Link>
                    </Button>
                  </Box>
                  {/* end date */}
                  <Box flex={1}>
                    {/* submit button */}
                    <Button
                      colorScheme="blue"
                      size="md"
                      variant="solid"
                      type="submit"
                      isLoading={isSubmitting}
                      type="submit"
                    >
                      Submit
                    </Button>
                  </Box>
                </HStack>
              </Wrap>
            </Stack>
          </form>
        </Box>
      </Center>
    </Layout>
  )
}

export default NewTask
