import React,{useRef} from 'react'
import Layout from '../layout/Master'
import {server} from '../../config/constants'
import axios from 'axios'
import moment from 'moment'
import { FaFileExcel } from 'react-icons/fa'
import { FaPlus } from 'react-icons/fa'
import { DownloadTableExcel } from 'react-export-table-to-excel';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Heading,
    Wrap,
    Button,
    Box,
    Badge
  } from '@chakra-ui/react'
  import { Link } from 'react-router-dom'
  import { useState, useEffect } from 'react'
import { log } from 'handlebars'
const Tasks = () => {

  const [tasks, setTasks] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const getTasks = async () => {
      setLoading(true)
      const tasksFromServer = await fetchTasks()
      setTasks(tasksFromServer)
      setLoading(false)
    }

    getTasks()
  }
  , [])

  // Fetch Tasks
  const fetchTasks = async () => {
    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    const res = await axios.get(`${server}/api/tasks`)
    return res.data.data
    
  }

  //Delete Task
  const deleteTask = async (id) => {
    console.log(id);
    alert('Are you sure you want to delete this task?')

    //passing token in header
    const token = localStorage.getItem('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    await axios.delete(`${server}/api/tasks/${id}`)
    setTasks(tasks.filter((task) => task._id !== id))
  }

  const tableRef = useRef(null);



  return (
    <Layout>    
        <Wrap spacing='30px' justify='space-between' align='center'>
        <Heading as="h1" size="xl" color="primary.500" mb={2}> Tasks </Heading>
        <Box>
        <DownloadTableExcel
                            filename="users table"
                            sheet="users"
                            currentTableRef={tableRef.current}
                        >
        <Button colorScheme="green" size="md" variant="solid" mr={2} leftIcon={<FaFileExcel />} variant='outline'>
            <Link to='#'>Export</Link>
        </Button>
        </DownloadTableExcel>
        <Button colorScheme="blue" size="md" variant="solid" leftIcon={<FaPlus />} variant='outline'>
            <Link to='/tasks/new'>Add Task</Link>
        </Button>
        </Box>
        </Wrap>
        <TableContainer>
  <Table variant='simple' border={'1px solid #ccc'} boxShadow={'sm'} borderRadius={'lg'} ref={tableRef}>
    <TableCaption>All Tasks List</TableCaption>
    <Thead>
      <Tr>
        <Th>Name</Th>
        <Th>Start Date</Th>
        <Th>End Date</Th>
        <Th>Assignees</Th>
        <Th>Projects</Th>
        <Th>Status</Th>
        <Th>Priority</Th>
        <Th>Actions</Th>
      </Tr>
    </Thead>
    <Tbody>
    {tasks.map((task, index) => (
        <Tr key={index}>
            <Td>{task.name}</Td>
            <Td>{moment(task.startdate).format('YYYY-MM-DD')}</Td>
            <Td>{moment(task.enddate).format('YYYY-MM-DD')}</Td>  
            <Td>{task.assignees && task.assignees.map((assignee, index) => (
                <span key={index}>{assignee.name}</span>
            ))}</Td>
            <Td>{task.projects && task.projects.map((project, index) => (
                <span key={index}>{project.name}</span>
            ))}</Td>
            <Td>
              <Badge colorScheme={
                task.status=='Pending' ?
               'blue':
               (task.status=='Completed'?'green':'purple') }>{task.status}</Badge>
            </Td>
            <Td>
            <Badge variant='outline'
            colorScheme={
                task.priority=='Low' ?
               'blue':
               (task.status=='High'?'Red':'green') }>{task.priority}</Badge>
            </Td>
            <Td>
                {/* <Button colorScheme="blue" size="sm" variant="solid">
                    <Link to={`/tasks/${task.id}`}>View</Link>
                </Button> */}
                <Button colorScheme="blue" size="sm" mr={2} variant="solid">
                    <Link to={`/tasks/${task._id}/edit`}>Edit</Link>
                    {/* <Link>Edit</Link> */}
                </Button>
                <Button colorScheme="red" size="sm" variant="solid" onClick={() => deleteTask(task._id)}>
                    <Link>Delete</Link>
                </Button>
            </Td>
        </Tr>
    ))}
    </Tbody>
    <Tfoot>
    <Tr>
        <Th>Name</Th>
        <Th>Start Date</Th>
        <Th>End Date</Th>
        <Th>Assignees</Th>
        <Th>Projects</Th>
        <Th>Status</Th>
        <Th>Priority</Th>
        <Th>Actions</Th>
      </Tr>
    </Tfoot>
  </Table>
</TableContainer>
    </Layout>
  )
}

export default Tasks
