# Task Management App

This repo contains backend and frontend implementation of Task Management App

## Usage

1. Rename the .envexample to .env and add your MONGO_URI and JWT SECRET
2. In Frontend/config/constants.js add your BACKEND_SERVER_URL
### Install dependencies

```
# Backend deps
npm install

# Frontend deps
cd frontend
npm install
```

### Run both Backend and Frontend
```
npm run dev
```
### Run Backend

```
npm run server
```
### Run Frontend

```
npm run client
```

### Seeding DB

```
in backend/seeder.js
1. add your MONGO_URI
2. run node seeder -i to import data
```

<br  /><br  />

# API Documentation



<!--- If we have only one group/collection, then no need for the "ungrouped" heading -->



## Endpoints

* [User management](#user-management)
    1. [register](#1-register)
    1. [Login](#2-login)
    1. [profile](#3-profile)
    1. [reset password](#4-reset-password)
    1. [logout](#5-logout)
    1. [request reset password](#6-request-reset-password)
* [Tasks](#tasks)
    1. [All Tasks](#1-all-tasks)
    1. [Single Task](#2-single-task)
    1. [Delete Task](#3-delete-task)
    1. [New Task](#4-new-task)
    1. [Update Task](#5-update-task)
* [Projects](#projects)
    1. [All Projects](#1-all-projects)
    1. [Single Project](#2-single-project)
    1. [New Project](#3-new-project)
    1. [update Project](#4-update-project)
    1. [delete Project](#5-delete-project)
    1. [Tasks](#6-tasks)
* [Assignees](#assignees)
    1. [All Assignees](#1-all-assignees)

--------



## User management

User Authentication



### 1. register



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| name | jules fabien |  |
| password | password |  |
| email | julesfabien32@gmail.com |  |
| phone | 0785126332 |  |



### 2. Login



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/users/login
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| password | password |  |
| email | julesfabien969@gmail.com |  |



### 3. profile



***Endpoint:***

```bash
Method: GET
Type: 
URL: 127.0.0.1:5000/api/users/profile
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |



### 4. reset password



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/users/reset-password
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| password | password |  |
| email | julesfabien969@gmail.com |  |



### 5. logout



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/users/logout
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| password | password |  |
| email | julesfabien969@gmail.com |  |



### 6. request reset password



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/users/request-reset-password
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| password | password |  |
| email | julesfabien969@gmail.com |  |



## Tasks

Task Crud Functionality



### 1. All Tasks



***Endpoint:***

```bash
Method: GET
Type: URLENCODED
URL: 127.0.0.1:5000/api/tasks
```



***Body:***



### 2. Single Task



***Endpoint:***

```bash
Method: GET
Type: 
URL: 127.0.0.1:5000/api/tasks/6565b0813a941e1eb7400fb6
```



### 3. Delete Task



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: 127.0.0.1:5000/api/tasks/6565b0813a941e1eb7400fb6
```



### 4. New Task



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/tasks
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| name | task 1 |  |
| startdate | 2020-12-12 |  |
| enddate | 2023-12-30 |  |
| description | Task #1 |  |
| priority | Normal |  |
| status | Pending |  |
| attachment | doc1.jpg |  |
| assignees | 656467701391a51829b1eec5 |  |
| assignees | 6564669dcfc80b40a9c3bc51 |  |
| projects | 6565be3bf703d2d512271501 |  |



### 5. Update Task



***Endpoint:***

```bash
Method: PUT
Type: URLENCODED
URL: 127.0.0.1:5000/api/tasks/6565b0813a941e1eb7400fb6
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Accept | application/json |  |
| Content-Type | application/json |  |



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| name | task 1 updated |  |
| startdate | 2020-12-12 |  |
| enddate | 2023-12-12 |  |
| description | hello world |  |
| priority | Normal |  |
| status | Completed |  |
| attachment | 1221.jpg |  |
| assignees | 656467701391a51829b1eec5 |  |
| project | 6321a3bdeb0fe86706433dc4 |  |
| assignees | 6564669dcfc80b40a9c3bc51 |  |
| project | 6321a3efeb0fe86706433dc7 |  |



## Projects

Project Crud Functionality



### 1. All Projects



***Endpoint:***

```bash
Method: GET
Type: URLENCODED
URL: 127.0.0.1:5000/api/projects
```



***Body:***



### 2. Single Project



***Endpoint:***

```bash
Method: GET
Type: 
URL: 127.0.0.1:5000/api/projects/6321a3b6eb0fe86706433dc1
```



### 3. New Project



***Endpoint:***

```bash
Method: POST
Type: URLENCODED
URL: 127.0.0.1:5000/api/projects
```



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| name | Project Name |  |
| description | test |  |



### 4. update Project



***Endpoint:***

```bash
Method: PUT
Type: URLENCODED
URL: 127.0.0.1:5000/api/projects/6321a3b6eb0fe86706433dc1
```



***Body:***


| Key | Value | Description |
| --- | ------|-------------|
| name | Main Project |  |
| description | test |  |



### 5. delete Project



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: 127.0.0.1:5000/api/projects/6321a3b6eb0fe86706433dc1
```



### 6. Tasks



***Endpoint:***

```bash
Method: GET
Type: 
URL: 
```



## Assignees

Assignees Crud Functionality



### 1. All Assignees



***Endpoint:***

```bash
Method: GET
Type: 
URL: 127.0.0.1:5000/api/users
```



---
[Back to top](#task-management-api)

>Generated at 2023-11-28 17:18:51 by [docgen](https://github.com/thedevsaddam/docgen)

